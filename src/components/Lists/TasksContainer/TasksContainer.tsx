import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import TasksList from '../../../models/TasksList';
import TasksListComponent from '../TasksList/TasksList';
import { Divider, Grid } from '@material-ui/core';
import AddTask from '../../Items/AddTask/AddTask'
import Task from '../../../models/Task';


const useStyles = makeStyles({
  root: {
    minWidth: 400,
  },
  title: {
    marginBottom: 20,
  },
  addTaskButton: {
    alignItems: 'center',
  }
});

export interface TasksContainerProps {list: TasksList}

export default function TasksContainer(props: TasksContainerProps) {
  const classes = useStyles();

  const [tasks, setNewTasks] = React.useState(props.list.tasks);

  function saveNewTask(newTask: Task) {
    setNewTasks([...tasks, newTask])
  }

  return (
      <Card className={classes.root}>
        <CardContent>
          <Grid container spacing={3}>
            <Grid item xs={9}>
            <Typography className={classes.title} variant="h3">
              {props.list.title}
            </Typography>
            </Grid>
            <Grid item xs={2} className={classes.addTaskButton}>
              <AddTask onSaveNewTask={(saveNewTask)}></AddTask>
            </Grid>
          </Grid>
          <Divider />
          <TasksListComponent tasks={tasks}/>
        </CardContent>
      </Card>
    
  );
}
