import React from 'react'
import { ThemeOptions, createMuiTheme } from "@material-ui/core";

declare module '@material-ui/core/styles/createMuiTheme' {
    interface Theme {

    }

    interface ThemeOptions {
    }
}

export default function createLennyTheme(options?: ThemeOptions): any {
    return createMuiTheme(options)
}
