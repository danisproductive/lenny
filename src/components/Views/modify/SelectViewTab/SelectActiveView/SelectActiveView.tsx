import React from 'react'
import SelectViewTab from "../SelectView"
import viewsStore from "../../../../../services/ViewsStore"

export default function SelectActiveView() {
    function updateActiveView(selectedViewId: number) {
        viewsStore.activeView = viewsStore.getView(selectedViewId)
    }

    return (
        <SelectViewTab onViewSelected={updateActiveView}></SelectViewTab>
    )
}