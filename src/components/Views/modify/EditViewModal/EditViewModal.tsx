import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import EditModal from '../../../App/BasicComponents/Windows/Modals/EditModal/EditModal'
import { TextField, Grid } from '@material-ui/core';
import viewsStore from '../../../../services/ViewsStore';
import ImportTabsToView from '../ImportTabsToView/ImportTabsToView';

interface EditViewModalProps {isOpenStatus: boolean, onDiscard: () => void}

export default function EditViewModal(props: EditViewModalProps) {
  const classes = useStyles()

  const onSaveView = () => {
    debugger;
  }

  return (
    <EditModal 
      classes={classes} 
      className={classes.root} 
      title="Edit view" 
      isOpenStatus={props.isOpenStatus} 
      onSave={onSaveView} 
      onDiscard={props.onDiscard}
    >
      <Grid container direction='column' spacing={3}>
        <Grid item xs={12}>
          <TextField id="view-name" label="View name" fullWidth/>
        </Grid>
        <Grid item xs={12}>
          <ImportTabsToView views={viewsStore.views}/>
        </Grid>
      </Grid>
    </EditModal>
  )
}

const useStyles = makeStyles((theme) => ({
    root: {
      minWidth: 500,
    },
}))

