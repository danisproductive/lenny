import React from 'react';
import ReactDOM from 'react-dom';
import ViewSelector from './ViewSelector';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ViewSelector />, div);
  ReactDOM.unmountComponentAtNode(div);
});