import TaskRelation from "./TaskRelation";
import taskRelationsStore from "../services/TaskRelationsStore";


export default class Task {
    private _id: number
    private _content: string
    private _taskRelation: TaskRelation
    private _relatedTask: Task
    [key: string]: any

    constructor(id: number = -1, content: string = "", relatedTask: Task = null, relation: TaskRelation = taskRelationsStore.taskRelations["none"]) {
        this._id = id
        this._content = content
        this._relatedTask = relatedTask
        this._taskRelation = relation
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get content(): string {
        return this._content;
    }

    set content(value: string) {
        this._content = value;
    }

    get relatedTask(): Task {
        return this._relatedTask;
    }

    get taskRelation(): TaskRelation {
        return this._taskRelation;
    }

    setTaskRelation(task: Task, taskRelation: TaskRelation) {
        this._taskRelation = taskRelation;
        this._relatedTask = task;
    }
}