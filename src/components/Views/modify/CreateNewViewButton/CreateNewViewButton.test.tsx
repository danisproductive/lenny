import React from 'react';
import ReactDOM from 'react-dom';
import CreateNewViewBotton from './CreateNewViewButton';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CreateNewViewBotton />, div);
  ReactDOM.unmountComponentAtNode(div);
});