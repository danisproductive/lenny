import React from 'react';
import ReactDOM from 'react-dom';
import ViewTabsNestedList from './ViewTabsNestedList';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ViewTabsNestedList />, div);
  ReactDOM.unmountComponentAtNode(div);
});