import React from 'react';
import { observer } from 'mobx-react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import { Collapse, makeStyles, SvgIconTypeMap, IconTypeMap } from '@material-ui/core';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

export interface SingleNestedListProps {title: string, icon?: React.ReactNode, isOpenByDefault?: boolean, children: React.ReactNode}

export default observer(function SingleNestedList(props: SingleNestedListProps) {
  const classes = useStyles()
  
  const [isListOpen, setOpen] = React.useState(props.isOpenByDefault);

  const handleListsToggle = () => {
    setOpen(!isListOpen);
  };

  return (
    <List className={classes.fullWidht}>
        <ListItem button onClick={handleListsToggle}>
        <ListItemIcon>
          {props.icon}
        </ListItemIcon>
        <ListItemText primary={props.title} />
        {isListOpen ? <ExpandLess /> : <ExpandMore />}
      </ListItem>
        <Collapse in={isListOpen} timeout="auto" unmountOnExit >
          {props.children}
        </Collapse>
    </List>
  )
})

const useStyles = makeStyles((theme) => ({
    fullWidht: { 
      width: '100%'
    }
  }
))