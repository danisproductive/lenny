import { observable } from 'mobx'

class UIStateStore {
    @observable private _isSidePanelOpen: boolean

    constructor() {
        this._isSidePanelOpen = false;
    }

    get isSidePanelOpen(): boolean {
        return this._isSidePanelOpen;
    }

    set isSidePanelOpen(newValue: boolean) {
        this._isSidePanelOpen = newValue;
    }

    toggleSidePanel() {
        this.isSidePanelOpen = !this.isSidePanelOpen
    }
}

var UIState = new UIStateStore()

export default UIState
