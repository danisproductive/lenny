import React from 'react';
import { InputLabel, Select, MenuItem, MenuProps } from '@material-ui/core';
import viewsStore from '../../../../services/ViewsStore';
import { makeStyles } from '@material-ui/core/styles';

interface SelectViewProps {onViewSelected: (selectedViewId: number) => void}

export default function SelectView(props: SelectViewProps) {
  const classes = useStyles()

  function onViewSelected(changeEvent: React.ChangeEvent<{ name?: string; value: unknown; }>) {
    const selectedViewId = changeEvent.target.value as number
    props.onViewSelected(selectedViewId)
  }

  return (
    <div className={classes.fullWidth}>
      <InputLabel id="view-select-label">View</InputLabel>
          <Select className={classes.fullWidth}
            labelId="view-select-label"
            id="view-select"
            value={viewsStore.activeView.id}
            onChange={onViewSelected}
            MenuProps={dropdownMenunProps}
          >
          {
            viewsStore.views.map((view) => {
              return <MenuItem key={view.id} value={view.id}>{view.name}</MenuItem>
            })
          }
          </Select>
      </div>
  )
}

const useStyles = makeStyles(() => ({
  fullWidth: {
    width: '100%'
  },
}))

const handleFocusExisted = () => {
  window.focus()
};

const dropdownMenunProps: Partial<MenuProps> = {
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left"
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "left"
  },
  getContentAnchorEl: null,
  onExited: handleFocusExisted
}

