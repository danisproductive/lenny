import { observable, computed } from 'mobx'
import TasksList from '../models/TasksList'
import Task from '../models/Task';
import React from "react";
import taskRelationsStore from "./TaskRelationsStore";

const t1 =  new Task(1, "a")
const t2 = new Task(2, "b")
const t3 = new Task(3, "c")

t1.setTaskRelation(t2, taskRelationsStore.taskRelations["parent"])
t2.setTaskRelation(t2, taskRelationsStore.taskRelations["child"])

const tmpLists:TasksList[] = [
    new TasksList(1, "Backlog", [t1,t2,t3]),
    new TasksList(2, "Active", [new Task(4, "a",), new Task(5, "b"), new Task(6, "casdasdasdasd")]),
    new TasksList(3, "Closed", [new Task(7, "a"), new Task(8, "b"), new Task(9, "c")]),
    new TasksList(4, "Temporary", [new Task(10, "aasdasdasdasdasd"), new Task(11, "b"), new Task(12, "casdasdasdasd")])
]

tmpLists.forEach(taskList => {
    taskList.tasks.forEach(task => {
        for (let additionalPropertyKey in task.taskRelation.additionalProperties) {
            task[additionalPropertyKey] = task.taskRelation.additionalProperties[additionalPropertyKey]
        }

        task.taskRelation.setRelation(task)
    })
})

class ListsStore {
    @observable private readonly _lists: TasksList[]
    @observable private readonly _listsFilter: number[]

    constructor() {
        this._lists = this.loadLists()
        this._listsFilter = []
    }

    get lists(): TasksList[] {
        return this._lists;
    }

    get listsFilter(): number[] {
        return this._listsFilter;
    }

    private loadLists(): TasksList[] {
        return tmpLists;
    }

    addList(newTaskList: TasksList): void {
        this._lists.push(newTaskList)
    }

    @computed get filteredLists() {
      return this._lists.filter(list => this._listsFilter.indexOf(list.id) === -1)
  }
}

let listsStore = new ListsStore()

export default listsStore
