import React from 'react';

import { GlobalHotKeys } from "react-hotkeys";
import UIState from '../../../services/UIStateStore';

export default function GlobalHotKeysHandler() {
    return (
      <GlobalHotKeys keyMap={keyMap} handlers={handlers} />
    )
}

const keyMap = {
  OPEN_SIDEPANEL: "ctrl+`",
};

const handlers = {
  OPEN_SIDEPANEL: () => {
    UIState.toggleSidePanel()
  }
};