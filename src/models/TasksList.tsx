import Task from './Task';
export default class TasksList {
    id: number
    title: string
    tasks: Task[]

    constructor (id:number = -1, title: string = "Unnamed Tasks List", tasks: Task[] = []) {
        this.id = id
        this.title = title
        this.tasks = tasks
    }
}