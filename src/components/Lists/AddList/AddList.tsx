import React from 'react';
import {
    Button,
    Fab,
    makeStyles,
    Slide, FormControl, InputLabel, Input, Tooltip, Modal, Typography, Grow
} from "@material-ui/core";
import Backdrop from '@material-ui/core/Backdrop';
import AddIcon from '@material-ui/icons/Add';
import green from "@material-ui/core/colors/green";
import {TransitionProps} from "@material-ui/core/transitions";
import listsStore from '../../../services/ListsStore';
import TasksList from '../../../models/TasksList';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #FFF',
        padding: theme.spacing(2, 4, 3),
        outline: 0,
        borderRadius: 5
    },
    addButton: {
        position: 'absolute',
        right: 40,
        bottom: 40,
        background: green[400],
        color: '#FAFAFA',
        '&:hover': {
            backgroundColor: green[300],
            color: '#FAFAFA'
        }
    }
}));

const Transition = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement<any, any> }, ref: React.Ref<unknown>) {
        return <Slide direction="left" ref={ref} {...props} />;
    }
);

let newList: TasksList

export default function AddList(props: any) {
    const classes = useStyles();

    const [openAddList, setOpenAddList] = React.useState(false);

    const handleOpen = () => {
        setOpenAddList(true);
        newList = new TasksList
    };

    const handleClose = () => {
        setOpenAddList(false);
    };

    const saveNewList = () => {
        listsStore.addList(newList)
        console.log(listsStore.lists)
        setOpenAddList(false);
    };

    return (
        <div>
            <Tooltip title="Add new list" aria-label="add" placement="bottom">
                <Fab className={classes.addButton} aria-label="add" onClick={handleOpen}>
                    <AddIcon/>
                </Fab>
            </Tooltip>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openAddList}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 200,
                }}
            >

        
            <Grow in={openAddList}>
                <div className={classes.paper}>
                    <Typography component="h2">
                        Create new list
                    </Typography>
                    <br />
                    <br />
                    <FormControl>
                        <InputLabel htmlFor="my-input">List name</InputLabel>
                        <Input autoFocus={true} id="my-input" onChange={(event) => newList.title = event.target.value}/>
                    </FormControl>
                    <br />
                    <br />
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={saveNewList} color="primary">
                        Save
                    </Button>
                </div>
            </Grow>
            </Modal>
        </div>
    )
}