import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core';
import SidePanelController from '../SidePanel/SidePanelController/SidePanelController';

export default observer(function Appbar() {
    const classes = useStyles();

    return (
      <div className={classes.root}>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <SidePanelController />
            <Typography variant="h6" noWrap>
              Lenny
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    )
})

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
}));
