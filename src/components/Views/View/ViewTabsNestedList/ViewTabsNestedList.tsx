import React from 'react';
import { List } from '@material-ui/core';
import VideoLabelIcon from '@material-ui/icons/VideoLabel';
import SingleNestedList from '../../../App/BasicComponents/Views/SingleNestedList/SingleNestedList';
import View from '../../../../models/View';
import TabToImport from '../../modify/ImportTabsToView/TabToImport/TabToImport';

interface ViewTabsNestedList {view: View}

export default function ViewTabsNestedList(props: ViewTabsNestedList) {
  return (
    <List >
      <SingleNestedList title={props.view.name} icon={<VideoLabelIcon />} >
        {
          props.view.tabs.map(tab => {
              return (<TabToImport tab={tab} />)
          })
        }
      </SingleNestedList>
    </List>
  )
}
