import React from 'react';
import ReactDOM from 'react-dom';
import GlobalSettings from './GlobalSettings';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GlobalSettings />, div);
  ReactDOM.unmountComponentAtNode(div);
});