import React from 'react';
import ReactDOM from 'react-dom';
import ImportTabsToView from './ImportTabsToView';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ImportTabsToView />, div);
  ReactDOM.unmountComponentAtNode(div);
});