import React from 'react';
import { Button, makeStyles, Grid, Typography } from "@material-ui/core";
import { WithStyles } from '@material-ui/core/styles';
import BasicModal from '../BasicModal/BasicModal';

interface EditModalkProps extends WithStyles {
    className?: string, 
    title:string, 
    isOpenStatus: boolean, 
    onSave: () => void, 
    onDiscard: () => void, 
    children: React.ReactNode
}

// TODO create omponent for titles to be used across the website, in order to kepp sizes identical across the ewbsite

export default function EditModal(props: EditModalkProps) {
    const classes = useStyles();

    return (
        <BasicModal isOpenStatus={props.isOpenStatus} closeModal={props.onDiscard}>
            <Grid container direction='column' spacing={2} className={props.classes.root || classes.container}>
                <Grid xs={12} item>
                    <Typography variant="h5">
                        {props.title}
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    {props.children}
                </Grid>
                <Grid container item xs={12} className={classes.buttons}>
                    <Grid item xs={2}>
                        <Button variant="contained" onClick={props.onSave} color="primary">
                            Save
                        </Button>
                    </Grid>
                    <Grid item xs={2}>
                        <Button variant="contained" onClick={props.onDiscard} color="secondary">
                            Discard
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </BasicModal>
    )
}

const useStyles = makeStyles((theme) => ({
    container: {
        minWidth: 300,
        minHeight: 150,
    },
    buttons: {
        display: 'flex',
        justifyContent: "flex-end"
    }
}));
