import React from 'react';
import { makeStyles,  Modal, Grow } from "@material-ui/core";
import Backdrop from '@material-ui/core/Backdrop';
import { HotKeys } from 'react-hotkeys';

interface BasicModalProps {isOpenStatus: boolean, closeModal: () => void, children: React.ReactNode}

export default function BasicModal(props: BasicModalProps) {
    const classes = useStyles();

    const SHORTCUTS_HANDLERS = {
        CLOSE_WINDOW: () => {
            props.closeModal();
        }
    }

    return (
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={props.isOpenStatus}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                    timeout: 200,
                }}
            >
            <Grow in={props.isOpenStatus}>
                <HotKeys handlers={SHORTCUTS_HANDLERS} keyMap={SHORTCUTS_KEYMAP}>
                    <div className={classes.paper}>
                        {props.children}
                    </div>
                </HotKeys>
            </Grow>
            </Modal>
        
    )
}

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(2, 4, 3),
        borderRadius: 5,
    },
    
}));

const SHORTCUTS_KEYMAP = {
    CLOSE_WINDOW: 'esc'
}