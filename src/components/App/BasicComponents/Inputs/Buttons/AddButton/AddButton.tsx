import React from 'react';
import { Tooltip, IconButton } from "@material-ui/core";
import AddIcon from '@material-ui/icons/Add';

interface AddButtonkProps {tooltip?: string, onClick: () => void}

export default function AddButton(props: AddButtonkProps) {
    return (
        <Tooltip title={props.tooltip} aria-label="add" placement="top">
            <IconButton aria-label="add" onClick={props.onClick}>
                <AddIcon/>
            </IconButton>
        </Tooltip>
    )
}