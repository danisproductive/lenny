import React from 'react';
import { ListItem, ListItemIcon, ListItemText, Checkbox, makeStyles, List } from '@material-ui/core';
import listsStore from '../../../services/ListsStore';

export default function TasksToggler() {
  const handleListToggle = (listID: number) => () => {
  const currentIndex = listsStore.listsFilter.indexOf(listID)

    if (currentIndex === -1) {
      listsStore.listsFilter.push(listID);
    } else {
      listsStore.listsFilter.splice(currentIndex, 1);
    }
  };

  const useStyles = makeStyles((theme) => ({
    nested: {
      paddingLeft: theme.spacing(4),
    },
  }));

  const classes = useStyles()

  return ( 
    <List component="div" disablePadding>
      {
        listsStore.lists.map((list) => {
          const labelId = `checkbox-list-label-${list.id}`;

          return (
            <ListItem button 
              key={list.id} 
              className={classes.nested}
              onClick={handleListToggle(list.id)}
            >
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={listsStore.listsFilter.indexOf(list.id) === -1}
                  tabIndex={-1}
                  disableRipple
                  inputProps={{ 'aria-labelledby': labelId }}
                />
              </ListItemIcon>
              <ListItemText primary={list.title} />
            </ListItem>
          )
        })
      }
    </List>
  )
}
