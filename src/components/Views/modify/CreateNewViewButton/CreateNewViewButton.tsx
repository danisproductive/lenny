import React, { useState } from 'react';
import AddButton from '../../../App/BasicComponents/Inputs/Buttons/AddButton/AddButton';
import EditViewModal from '../EditViewModal/EditViewModal';

export default function CreateNewViewButton() {
  const [isModalOpen, setIsModalOpen] = useState(false)

  const openCreateViewModal = () => {
    setIsModalOpen(true);
  }

  const closeCreateViewModal = () => {
    setIsModalOpen(false);
  }

  return (
    <div>
      <AddButton tooltip="Create new view" onClick={openCreateViewModal}></AddButton>
      <EditViewModal isOpenStatus={isModalOpen} onDiscard={closeCreateViewModal}></EditViewModal>
    </div>
  )
}
