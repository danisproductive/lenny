import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Divider,  Card, CardContent } from '@material-ui/core';
import View from '../../../../models/View';
import ViewTabsNestedList from '../../View/ViewTabsNestedList/ViewTabsNestedList';

interface ImportTabsToViewProps {views: View[]}

export default function ImportTabsToView(props: ImportTabsToViewProps) {
  const classes = useStyles()

  const [imported, setImported] = React.useState([0]);

  const handleIsImportedClick = (value: number) => () => {
    const currentIndex = imported.indexOf(value);
    const newImported = [...imported];

    if (currentIndex === -1) {
      newImported.push(value);
    } else {
      newImported.splice(currentIndex, 1);
    }

    setImported(newImported);
  };

  const [importTypes, setImportType] = React.useState([0]);

  const handleImportTypeClick = (value: number) => () => {
    const currentIndex = importTypes.indexOf(value);
    const newChecked = [...importTypes];

    if (currentIndex === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(currentIndex, 1);
    }

    setImportType(newChecked);
  };

  return (
    <Card className={classes.root} elevation={3}>
      <CardContent>
        <Typography variant="h5" component="h2">
          Import existing tabs
        </Typography>
        <Typography className={classes.subtext} color="textSecondary">
          Choose which tabs to import to your new view, and choose to copy them or reference the same tab
        </Typography>
        <Divider />
        <div className={classes.viewsTabs}>
          {props.views.map((view => {
            return (
              <ViewTabsNestedList view={view}></ViewTabsNestedList>
            )
          }))}
        </div>
      </CardContent>
    </Card>
  )
}

const useStyles = makeStyles((theme) => ({
    root: {
      minWidth: '30vh',
      maxHeight: '60vh',
    },
    viewsTabs: {
      maxHeight: '40vh',
      overflowY: 'auto',
    },
    subtext: {
      marginBottom: 12,
    },
}))

