import React from 'react';
import Appbar from './components/App/Appbar/Appbar';
import { observer } from 'mobx-react';
import { makeStyles } from '@material-ui/core';
import Toolbar from '@material-ui/core/Toolbar';
import clsx from 'clsx';
import UIState from './services/UIStateStore';
import SidePanel, { drawerWidth } from './components/App/SidePanel/SidePanel';
import TasksContainers from './components/Lists/TasksContainers/TasksContainers';
import listsStore from './services/ListsStore';
import GlobalSettings from './components/App/GlobalSettings/GlobalSettings';

export default observer(function App() {
  const classes = useStyles();

  return (
    <GlobalSettings>
      <Appbar />
      <SidePanel />
      <main className={clsx(classes.content, {[classes.contentShift]: UIState.isSidePanelOpen,})}>
        <Toolbar />
        <TasksContainers lists={listsStore.filteredLists}/>
      </main>
    </GlobalSettings>
  );
})

const useStyles = makeStyles((theme) => ({
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeIn,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  contentShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeIn,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
}))