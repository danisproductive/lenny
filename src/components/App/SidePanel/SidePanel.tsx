import React from 'react';
import Drawer from '@material-ui/core/Drawer';
import { makeStyles } from '@material-ui/core/styles';
import { observer } from 'mobx-react-lite'
import Toolbar from '@material-ui/core/Toolbar';
import UIState from '../../../services/UIStateStore';
import ViewSelector from '../../Views/View/ViewSelector/ViewSelector';
import { List, ListItem } from '@material-ui/core';
import CreateNewViewButton from '../../Views/modify/CreateNewViewButton/CreateNewViewButton';

export const drawerWidth = 240;

export default observer(function SidePanel() {
  const classes = useStyles();

  return (
    <Drawer
      className={classes.drawer}
      variant="persistent"
      anchor="top"
      open={UIState.isSidePanelOpen}
      classes={{paper: classes.drawerPaper}}
    >
    <Toolbar />
      <div className={classes.drawerContainer}>
        <List className={classes.list}>
          <ListItem>
            <ViewSelector /> 
            <CreateNewViewButton />
          </ListItem>
        </List>
      </div>
    </Drawer>
)
})

// <ListItem className={classes.list}><ListsList /></ListItem>

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100%',
    boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
  },
  drawerContainer: {
    overflow: 'auto',
    width: '100%',
    height: '100%'
  },
  list: {
    width: '100%'
  }
}));
