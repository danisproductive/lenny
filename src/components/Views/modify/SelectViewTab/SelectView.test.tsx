import React from 'react';
import ReactDOM from 'react-dom';
import SelectViewTab from './SelectView';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SelectViewTab onViewSelected={() => {}} />, div);
  ReactDOM.unmountComponentAtNode(div);
});