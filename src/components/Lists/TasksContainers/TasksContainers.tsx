import React from 'react';
import TasksContainer, { TasksContainerProps } from '../TasksContainer/TasksContainer'
import { Grid, makeStyles, Box } from '@material-ui/core';
import TasksList from '../../../models/TasksList';
import AddList from "../AddList/AddList";

const useStyles = makeStyles({
  grid: {
    marginBottom: 20,
    overflow: 'auto',
    whiteSpace: 'nowrap',
    margin: 0,
    padding: 0,
    listStyle: "none",
    height: "100%",
  },
  gridItem: {
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 20,
    display: 'inline-block',
  }
});

export interface TasksContainersProps {lists: TasksList[]}

export default function TasksContainers(props: TasksContainersProps) {
  const classes = useStyles();

  return (
    <div>
      <Box className={classes.grid}>
          {
            props.lists.map(list => {
              return (
                <Box key={list.id} className={classes.gridItem}>
                  <TasksContainer list={list}/>
                </Box>
                )
            })
          }
        </Box>
      <AddList/>
    </div>
  );
}
