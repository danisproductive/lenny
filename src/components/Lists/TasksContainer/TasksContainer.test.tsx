import React from 'react';
import ReactDOM from 'react-dom';
import TasksContainer from './TasksContainer';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TasksContainer />, div);
  ReactDOM.unmountComponentAtNode(div);
});