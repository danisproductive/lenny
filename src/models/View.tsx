import Tab from './Tab';

export default class View {
    private _id: number
    private _name: string
    private _tabs: Tab[]

    [key: string]: any

    constructor(id: number = -1, name: string = "",tabs: Tab[] = []) {
        this._id = id
        this._name = name
        this._tabs = tabs
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get tabs(): Tab[] {
        return this._tabs;
    }
}