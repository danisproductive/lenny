import { observable } from 'mobx'
import Tab from '../models/Tab';

const tmpViews: Tab[] = [
    new Tab(1, "Week1"),
    new Tab(2, "Week2"),
    new Tab(3, "Week3")
]

class TabsStore {
    @observable private readonly _tabs: Tab[]
    @observable private _activeTab: Tab

    constructor() {
        this._tabs = this.loadTabs()
        this._activeTab = this._tabs[0]
    }

    get tabs(): Tab[] {
        return this._tabs;
    }

    private loadTabs(): Tab[] {
        return tmpViews;
    }

    getTab(id: number): Tab {
        return this._tabs.find((tab) => tab.id === id)
    }

    addTab(newTab: Tab): void {
        this._tabs.push(newTab)
    }

    get activeTab(): Tab {
        return this._activeTab;
    }

    set activeTab(newValue: Tab) {
        this._activeTab = newValue;
    }
}

let tabsStore = new TabsStore()

export default tabsStore
