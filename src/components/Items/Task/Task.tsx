import React from 'react';
import TaskModel from "../../../models/Task";

export interface TaskProps { task: TaskModel; }

export default function Task(props: TaskProps) {
  return (
      <div>
          { props.task.taskRelation.display(props.task) }
      </div>
  );
}
