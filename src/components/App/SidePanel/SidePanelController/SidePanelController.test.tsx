import React from 'react';
import ReactDOM from 'react-dom';
import SidePanelController from './SidePanelController';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SidePanelController />, div);
  ReactDOM.unmountComponentAtNode(div);
});