import React from 'react';
import { observer } from 'mobx-react';
import { InputLabel, Select, MenuItem, MenuProps } from '@material-ui/core';
import viewsStore from '../../../../services/ViewsStore';
import { makeStyles } from '@material-ui/core/styles';
import View from '../../../../models/View';

export default observer(function ViewSelector() {
  const classes = useStyles()

  function updateActiveView(changeEvent: React.ChangeEvent<{ name?: string; value: unknown; }>) {
    viewsStore.activeView = changeEvent.target.value as View
  }

  return (
    <div className={classes.fullWidth}>
      <InputLabel id="view-select-label">View</InputLabel>
          <Select className={classes.fullWidth}
            labelId="view-select-label"
            id="view-select"
            value={viewsStore.activeView}
            onChange={updateActiveView}
            MenuProps={dropdownMenunProps}
          >
          {
            viewsStore.views.map((view) => {
              return <MenuItem 
                  key={view.id} 
                  // @ts-ignore [1]
                  value={view}>
                    {view.name}
                </MenuItem>
            })
          }
          </Select>
      </div>
  )

  // [1]:
  // No overload matches this call.
  // The last overload gave the following error.
  //   Type 'View' is not assignable to type 'string | number | string[]'.
  //     Type 'View' is not assignable to type 'string'.ts(2769)
})

const useStyles = makeStyles(() => ({
  fullWidth: {
    width: '100%'
  },
}))

const dropdownMenunProps: Partial<MenuProps> = {
  anchorOrigin: {
    vertical: "bottom",
    horizontal: "left"
  },
  transformOrigin: {
    vertical: "top",
    horizontal: "left"
  },
  getContentAnchorEl: null,
}

