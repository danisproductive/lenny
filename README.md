# Lenny

A task management application written in React with MobX\
With focus on user customizability and cross-project tasks

### HTML_Export.... .zip
An export form MockFlow. The UI design of the application

### DB.png
Displays the data relations in the system

### System usecases.png
Displaying some of the usecases and how they are handled

### Item types and relations problems and solutions.docx
A file describing some of the difficulties in the user customizability options\
and how they are solved


## Note
This is a WIP and has not been worked on for a while.

## Running the project

Run npm install, then npm start to run the app in development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


