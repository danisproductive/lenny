import Task from "./Task";
import {AnyObject} from "./SystemWideInterfaces";

export default class TaskRelation {
    name: string
    additionalProperties: AnyObject
    setRelation: (task: Task) => void
    display: (task: Task) => JSX.Element

    constructor(name: string, additionalProperties: AnyObject, setRelation: (task: Task) => void, display: (task: Task) => JSX.Element) {
        this.name = name
        this.additionalProperties = additionalProperties
        this.setRelation = setRelation
        this.display = display
    }
}