import React from 'react';
import ReactDOM from 'react-dom';
import TasksToggler from './TasksToggler';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TasksToggler />, div);
  ReactDOM.unmountComponentAtNode(div);
});