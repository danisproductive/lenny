import React from 'react';
import ReactDOM from 'react-dom';
import SelectActiveView from './SelectActiveView';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SelectActiveView />, div);
  ReactDOM.unmountComponentAtNode(div);
});