import React from 'react';
import ReactDOM from 'react-dom';
import EditModal from './EditModal';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EditModal />, div);
  ReactDOM.unmountComponentAtNode(div);
});