import React from 'react';
import { observer } from 'mobx-react';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import TasksToggler from '../TasksToggler/TasksToggler';
import SingleNestedList from '../../App/BasicComponents/Views/SingleNestedList/SingleNestedList';

export default observer(function ListsList() {
  return (
    <SingleNestedList title="Lists" icon={<InboxIcon />}>
      <TasksToggler />
    </SingleNestedList>
  )
})
