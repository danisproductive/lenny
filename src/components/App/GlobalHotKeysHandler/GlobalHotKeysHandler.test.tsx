import React from 'react';
import ReactDOM from 'react-dom';
import GlobalHotKeysHandler from './GlobalHotKeysHandler';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<GlobalHotKeysHandler />, div);
  ReactDOM.unmountComponentAtNode(div);
});