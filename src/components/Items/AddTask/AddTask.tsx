import React from 'react';
import { Button, makeStyles, FormControl, Tooltip, Modal, Grow, IconButton, TextField } from "@material-ui/core";
import Backdrop from '@material-ui/core/Backdrop';
import AddIcon from '@material-ui/icons/Add';
import Task from '../../../models/Task';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paper: {
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #FFF',
        padding: theme.spacing(2, 4, 3),
        outline: 0,
        borderRadius: 5
    },
    
}));

let newTask: Task

interface AddTaskProps {onSaveNewTask: (task: Task) => void}

export default function AddTask(props: AddTaskProps) {
    const classes = useStyles();

    const [openAddList, setOpenAddList] = React.useState(false);

    const handleOpen = () => {
        setOpenAddList(true);
        newTask = new Task
    };

    const handleClose = () => {
        setOpenAddList(false);
    };

    const saveNewList = () => {
        props.onSaveNewTask(newTask)
        setOpenAddList(false);
    };

    return (
        <div>
            <Tooltip title="Add new task" aria-label="add" placement="top">
              <IconButton aria-label="add" onClick={handleOpen}>
                <AddIcon/>
              </IconButton>
            </Tooltip>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                className={classes.modal}
                open={openAddList}
                onClose={handleClose}
                closeAfterTransition
                BackdropComponent={Backdrop}
                BackdropProps={{
                timeout: 200,
                }}
            >
            <Grow in={openAddList}>
                <div className={classes.paper}>
                    <FormControl>
                        <TextField
                          id="standard-full-width"
                          style={{ margin: 8 }}
                          placeholder="New task"
                          fullWidth
                          margin="normal"
                          InputLabelProps={{
                            shrink: true,
                          }}
                          variant="filled"
                          onChange={(event) => newTask.content = event.target.value}
                        />
                    </FormControl>
                    <Button onClick={saveNewList} color="primary">
                        Save
                    </Button>
                </div>
            </Grow>
            </Modal>
        </div>
    )
}