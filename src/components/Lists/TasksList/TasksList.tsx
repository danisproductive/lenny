import React from 'react';
import { List } from '@material-ui/core';
import TaskComponent from '../../Items/Task/Task'
import Task from '../../../models/Task';


export interface TasksProps {tasks: Task[]; }

export default function TasksList(props: TasksProps) {
  return (
    <List component="nav" aria-label="main mailbox folders">
    {
      props.tasks.map(task => {
        return <TaskComponent key={task.id} task={task}/>
      })
    }
    </List>
  )
};
