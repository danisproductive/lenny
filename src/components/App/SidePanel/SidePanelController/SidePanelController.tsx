import React from 'react';
import IconButton from '@material-ui/core/IconButton';
import { observer } from 'mobx-react';
import UIState from '../../../../services/UIStateStore';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';


export default observer(function SidePanelController() {
    const toggleSidePanel = () => {
        UIState.toggleSidePanel()
    }
  
    return (
      <IconButton
        color="inherit"
        aria-label="open drawer"
        onClick={toggleSidePanel}
        edge="start"
      >
        {UIState.isSidePanelOpen ? <ExpandLess /> : <ExpandMore />}
      </IconButton>
    )
})
