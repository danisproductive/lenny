import React from 'react';
import ReactDOM from 'react-dom';
import BasicModal from './BasicModal';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<BasicModal isOpenStatus={true}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});