import React from 'react';
import ReactDOM from 'react-dom';
import ListsList from './ListsList';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ListsList />, div);
  ReactDOM.unmountComponentAtNode(div);
});