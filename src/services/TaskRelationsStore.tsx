import { observable } from 'mobx'
import Task from '../models/Task';
import React from "react";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";
import {Box, Collapse} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import CreateIcon from "@material-ui/icons/Create";
import TaskRelation from "../models/TaskRelation";

interface TaskRelationsObject {[key:string]: TaskRelation}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        maxWidth: 360,
        backgroundColor: theme.palette.background.paper,
    },
}));

const taskRelations: TaskRelationsObject = {
    child: new TaskRelation("child", {parentID: -1}, (task: Task) => { task.parentID = task.relatedTask.id }, (task: Task) => { return null }),
    parent: new TaskRelation(
        "parent",
        {childTasks: []},
        (task: Task) => {
            task.childTasks.push(task.relatedTask)
        },
        (task: Task) => {
            const useStyles = makeStyles((theme) => ({
                nested: {
                    paddingLeft: theme.spacing(4),
                },
            }));

            const classes = useStyles()

            return (
                <div>
                    <ListItem key={task.id}>
                        <ListItemIcon>
                            <CreateIcon/>
                        </ListItemIcon>
                        <ListItemText primary={task.content} />
                    </ListItem>
                    <Collapse in={true} timeout="auto" unmountOnExit addEndListener={node => {}}>
                        {
                            task.childTasks.map((childTask: Task) => {
                                return (<Box className={classes.nested}>
                                    <ListItem key={task.id}>
                                        <ListItemIcon>
                                            <CreateIcon/>
                                        </ListItemIcon>
                                        <ListItemText primary={childTask.content} />
                                    </ListItem>
                                </Box>)
                            })
                        }
                    </Collapse>
                </div>
            )
        }
    ),
    none: new TaskRelation(
        "none", {}, (task: Task) => {},
        (task: Task) => {
            const classes = useStyles();

            return (
                <div className={classes.root}>
                    <ListItem button>
                        <ListItemIcon>
                            <CreateIcon/>
                        </ListItemIcon>
                        <ListItemText primary={task.content} />
                    </ListItem>
                </div>
            )
        }
    )
}

class TaskRelationsStore {
    @observable taskRelations: TaskRelationsObject = taskRelations

    constructor() {
        this.taskRelations = this.loadRelations()
    }

    loadRelations(): TaskRelationsObject {
        return taskRelations;
    }
}

let taskRelationsStore = new TaskRelationsStore()

export default taskRelationsStore

