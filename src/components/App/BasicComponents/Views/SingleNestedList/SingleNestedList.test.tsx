import React from 'react';
import ReactDOM from 'react-dom';
import SingleNestedList from './SingleNestedList';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SingleNestedList title="test" nestedListContent={<div>test</div>} />, div);
  ReactDOM.unmountComponentAtNode(div);
});