import React from 'react';
import { ThemeProvider } from '@material-ui/core';
import { indigo, amber } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';
import GlobalHotKeysHandler from '../GlobalHotKeysHandler/GlobalHotKeysHandler';
import createLennyTheme from '../Style/Theme/LennyTheme';
import { PropTypes } from 'mobx-react';
import { makeStyles } from '@material-ui/core/styles';

export interface GlobalSettingsProps {children: React.ReactNode}

export default function GlobalSettings(props: GlobalSettingsProps) {
  useStyles()

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <GlobalHotKeysHandler />
      {props.children}
    </ThemeProvider>
  )
}

const darkTheme = createLennyTheme({
  palette: {
    type: 'dark',
    primary: indigo,
    secondary: amber,
  },
});

const useStyles = makeStyles(() => {
  return {
    '@global': {
      '*::-webkit-scrollbar': {
        width: '0.4em'
      },
      '*::-webkit-scrollbar-track': {
        boxShadow: 'inset 0 0 0 rgba(0,0,0,0.00)',
        webkitBoxShadow: 'inset 0 0 0 rgba(0,0,0,0.00)'
      },
      '*::-webkit-scrollbar-thumb': {
        backgroundColor: '#3f51b5',
        outline: '1px solid slategrey'
      }
    }
  }
})
