import React from 'react';
import ReactDOM from 'react-dom';
import TasksContainers from './TasksContainers';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TasksContainers />, div);
  ReactDOM.unmountComponentAtNode(div);
});