import React from 'react';
import ReactDOM from 'react-dom';
import TabToImport from './TabToImport';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TabToImport />, div);
  ReactDOM.unmountComponentAtNode(div);
});