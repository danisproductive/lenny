export default class Tab {
    private _id: number
    private _name: string
    private _parentViewID: number

    [key: string]: any

    constructor(id: number = -1, name: string = "", parentViewID: number = -1) {
        this._id = id
        this._name = name
        this._parentViewID = parentViewID
    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get parentViewID(): number {
        return this._parentViewID;
    }

    set parentViewID(value: number) {
        this._parentViewID = value;
    }
}