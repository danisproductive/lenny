import React from 'react';
import { Checkbox, Switch, Tooltip, ListItemSecondaryAction } from '@material-ui/core';
import LinkOutlinedIcon from '@material-ui/icons/LinkOutlined';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';
import Tab from '../../../../../models/Tab';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { makeStyles } from '@material-ui/core/styles';

interface TabToImport {tab: Tab}

export default function TabToImport(props: TabToImport) {
  const classes = useStyles()
  const tabId = props.tab.id
  let [isImported, setIsImported] = React.useState(false)
  let [isCopy, setIsCopy] = React.useState(false)

  return (
    <ListItem key={'tab-list-item-' + tabId} dense button >
      <ListItemIcon>
        <Checkbox
          edge="start"
          checked={isImported}
          onChange={() => setIsImported(!isImported)}
          tabIndex={-1}
          disableRipple
        />
      </ListItemIcon>
      <ListItemText primary={props.tab.name} />
      <ListItemSecondaryAction>
        <Tooltip title={isCopy ? "Copy current contents of the tab" : "Use the same tab that will update in both views"}>
          <div className={classes.importType}>
            <Switch
              edge="end"
              onChange={() => {setIsCopy(!isCopy)}}
              checked={isCopy}
              disabled={!isImported}
            />
            {isCopy ? <FileCopyOutlinedIcon /> : <LinkOutlinedIcon />}
          </div>
        </Tooltip>
      </ListItemSecondaryAction>
    </ListItem>
  )
}

const useStyles = makeStyles(() => {
  return {
    importType: {
      display: 'flex',
      alignItems: 'center'
    }
  }
})
