import React from 'react';
import ReactDOM from 'react-dom';
import EditViewModal from './EditViewModal';

it('It should mount', () => {
  const div = document.createElement('div');
  ReactDOM.render(<EditViewModal />, div);
  ReactDOM.unmountComponentAtNode(div);
});