import { observable } from 'mobx'
import View from '../models/View';
import Tab from '../models/Tab';

const tmpViews: View[] = [
    new View(1, "Tasks", [new Tab(1, "Week 1", 1), new Tab(2, "Week 2", 1), new Tab(3, "Week 3", 1)]),
    new View(2, "Retro", [new Tab(1, "Week 1", 2), new Tab(5, "Week 2", 2), new Tab(6, "Week 4", 2)]),
    new View(3, "Daily", [new Tab(7, "Day 1", 3), new Tab(8, "Day 2", 3), new Tab(9, "Day 3", 3), new Tab(10, "Day 4", 3), new Tab(11, "Day 5", 3), new Tab(12, "Day 7", 3), new Tab(13, "Day 6", 3)])
]

class ViewsStore {
    @observable private readonly _views: View[]
    @observable private _activeView: View

    constructor() {
        this._views = this.loadViews()
        this._activeView = this._views[0]
    }

    get views(): View[] {
        return this._views;
    }

    private loadViews(): View[] {
        return tmpViews;
    }

    getView(id: number): View {
        return this._views.find((view) => view.id === id)
    }

    addView(newView: View): void {
        this._views.push(newView)
    }

    get activeView(): View {
        return this._activeView;
    }

    set activeView(newValue: View) {
        this._activeView = newValue;
    }
}

let viewsStore = new ViewsStore()

export default viewsStore
